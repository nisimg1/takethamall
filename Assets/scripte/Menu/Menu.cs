﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [Required("you must put the PreviwGameObject")]
    public GameObject PreviwGameObject;

  private void Start()
  {
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
  }

  void Update()
    {
        PreviwGameObject.transform.Rotate(Vector3.up *100 * Time.deltaTime);
    }

    public void OnButtonPress(CanvasGroup canvasGroup)
    {
        Show(canvasGroup);
    }   

    public void OnExit()
    {
        Application.Quit();
    }

    void Show(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
    }

    public void Hide(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
    }
}
