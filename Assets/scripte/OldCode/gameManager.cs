using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace OldCode
{
  public class gameManager : MonoBehaviour
  {
    public static gameManager Instance;
    [SerializeField] GameData curData;

    [SerializeField] GameObject chert;
    [SerializeField] GameObject endSessionPanel;
    [SerializeField] GameObject puasePanel;
    [SerializeField] GameObject musicGenreListContainer;
    [SerializeField] Button[] musicGenreBtns;
    [SerializeField] GameObject[] genreDropdownsGameObject;
    [SerializeField] Dropdown[] genreDropdowns;

    [SerializeField] AudioClip[] harmonySounds;
    [SerializeField] AudioClip[] BPMSounds;

    [SerializeField] AudioSource harmonySource;
    [SerializeField] AudioSource BPMSource;

    int currentGenre;
    int currentGenreRangeTest = 0;
    int currentBPMRangeTest = 0;

    int currentMode = 1;

    string userName = "User";

    public bool firstShot = false;
    public bool IsPause { get { return puasePanel.activeInHierarchy; } }

    [SerializeField] float sessionTime = 30f;
    [SerializeField] bool isRangeSession = false;

    void Awake()
    {
      Instance = this;
      if (chert != null) chert.SetActive(false);
      if(puasePanel != null) puasePanel.SetActive(false);
    }

    void Start()
    {
      if (isRangeSession)
        StartCoroutine(SessionManager());
      for (int i = 0; i < musicGenreBtns.Length; i++)
      {
        if (musicGenreBtns == null) return;
        musicGenreBtns[i].colors = GetGenreButtonStyle(i == 0);
        genreDropdownsGameObject[i].SetActive(i == 0);
      }
    }


    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Escape))
      {
        if (puasePanel != null)
          puasePanel.SetActive(!puasePanel.activeInHierarchy);
      }
    }

    IEnumerator SessionManager()
    {
      yield return new WaitForSeconds(sessionTime);
      Time.timeScale = 0;
      endSessionPanel.SetActive(true);
      Cursor.lockState = CursorLockMode.None;
      Cursor.visible = true;
    }

    public void SetUserName(string name)
    {
      userName = name;
    }

    public void SetBPMAndGenreIndex(int genre, int bpm)
    {
      currentBPMRangeTest = bpm;
      currentGenreRangeTest = genre;
    }

    bool changeBPM = true;
    public void ChangeToRandomMusic()
    {
      if (!changeBPM)
      {
        harmonySource.Stop();
        harmonySource.clip = harmonySounds[UnityEngine.Random.Range(0, harmonySounds.Length - 1)];
        harmonySource.Play();
        changeBPM = true;
      }
      else
      {
        BPMSource.Stop();
        BPMSource.clip = BPMSounds[UnityEngine.Random.Range(0, BPMSounds.Length - 1)];
        BPMSource.Play();
        changeBPM = false;
      }
    }

    public void ToggleMusicGenre(int genreIndex)
    {
      currentGenre = genreIndex;
      Debug.Log("current genre: " + currentGenre);
      for (int i = 0; i < musicGenreBtns.Length; i++)
        musicGenreBtns[i].colors = GetGenreButtonStyle(i == genreIndex);
      OpenGenreDropDown(genreIndex);
    }

    ColorBlock GetGenreButtonStyle(bool isSelected)
    {
      ColorBlock colorBlock = new ColorBlock();
      colorBlock.normalColor = new Color(1, 1, 1, isSelected ? 1f : 0.25f);
      colorBlock.highlightedColor = new Color(1, 1, 1, isSelected ? 1f : 0.5f);
      colorBlock.pressedColor = new Color(0.8f, 0.8f, 0.8f, isSelected ? 1f : 0.5f);
      colorBlock.selectedColor = colorBlock.normalColor;
      colorBlock.disabledColor = new Color(1, 1, 1, 0);
      colorBlock.colorMultiplier = 1;
      colorBlock.fadeDuration = 0.2f;
      return colorBlock;
    }

    void OpenGenreDropDown(int genre)
    {
      for (int i = 0; i < genreDropdownsGameObject.Length; i++)
        genreDropdownsGameObject[i].SetActive(i == genre);
    }

    public void ToggleBPM(int bpmIndex)
    {
      BPMSource.Stop();
      harmonySource.Stop();
      Debug.Log("genre offset : " + (currentGenre * 8).ToString() + " dropdown value: " + genreDropdowns[bpmIndex].value);
      BPMSource.clip = BPMSounds[(currentGenre * 8) + genreDropdowns[bpmIndex].value];
      harmonySource.clip = harmonySounds[(currentGenre * 8) + genreDropdowns[bpmIndex].value];
      harmonySource.Play();
      BPMSource.Play();
    }

    public void ToggleMusicGenreContainer()
    {
      musicGenreListContainer.SetActive(!musicGenreListContainer.activeInHierarchy);
    }

    public void ShotHit()
    {
      if (currentMode >= 0)
      {
        curData._shotHit++;
        AccuracyTest();
      }
    }

    public void ShotMiss()
    {
      if (currentMode >= 0)
      {
        curData._shotMiss++;
        AccuracyTest();
      }
    }

    public void AccuracyTest()
    {
      var tergetOverAll = curData._shotHit + curData._shotMiss;
      curData._accuracy = curData._shotHit / tergetOverAll;
      curData._accuracy *= 100;
      Debug.Log(tergetOverAll);
    }

    public float GetAccuracyRate() => curData._accuracy;
    public float GetNumOfTarget() => curData._shotHit;

    void SumUpSession()
    {
      if (target.instance == null) return;

      float targetOverall = curData._shotMiss + curData._shotHit;
      float ttk = target.instance.ttkC();

      ProduceSessionInfoFile(targetOverall, ttk);

      curData.ResetData();
      target.instance.Clearlist();

      Debug.Log("ttk :" + ttk);
    }

    void ProduceSessionInfoFile(float targetOverall, float ttk)
    {
      string path = Path.Combine(Application.dataPath, "StreamingAssets", $"{userName + DateTime.Now.ToString("MMMM dd")}.txt");
      string txtToWrite = $"Target Overall: {targetOverall} Accuracy : {curData._accuracy} TTK: {ttk} Genre: {currentGenreRangeTest} BPM: {currentBPMRangeTest} \n";

      if (!File.Exists(path))
        File.WriteAllText(path, txtToWrite);
      else
        File.AppendAllText(path, txtToWrite);
    }

    public void OnStartGameClicked()
    {
      Time.timeScale = 1;
      SceneManager.LoadScene("OfficeDemo");
    }

    public void OnReplayClicked()
    {
      Time.timeScale = 1;
      SceneManager.LoadScene("Demo_Bunker");
    }

    public void GoToMainMenu()
    {
      SceneManager.LoadScene("MainMenu");
    }

    void OnDestroy()
    {
      SumUpSession();
    }
  }
}