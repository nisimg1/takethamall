using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace OldCode
{
  public class target : MonoBehaviour
  {
    public Transform[] RandomPoint;
    public static target instance;

    [SerializeField] AudioClip _hitMarker;
    [SerializeField] AudioSource _audioSource;

    [SerializeField] private GameObject dmDown;
    float timeBetweenShots;
    List<float> timeBetweenShotsList = new List<float>();
    float m = 0;

    float Nam;
    private void Awake()
    {
      instance = this;

    }
    private void Update()
    {
      timeBetweenShots += Time.deltaTime;
    }
    public void getHit()
    {
      Vector3 myPos = transform.position;
      Debug.Log("I AM STACK HERE");
      var randomPoint = Random.Range(0, RandomPoint.Length);
      var addTime = timeBetweenShots;
      timeBetweenShotsList.Add(addTime);
      timeBetweenShots = 0;
      transform.position = new  Vector3(Random.Range(RandomPoint[0].position.x,RandomPoint[1].position.x) ,Random.Range(RandomPoint[0].position.y,RandomPoint[1].position.y),
        Random.Range(RandomPoint[0].position.z,RandomPoint[1].position.z));
      var tamp = Instantiate(dmDown);
      tamp.transform.position = myPos;
      _audioSource.PlayOneShot(_hitMarker);

    }

    public float ttkC()
    {
      m = 0;
      foreach (var item in timeBetweenShotsList)
      {
        m += item;
      }
      //Debug.Log(m);
      float average = m / timeBetweenShotsList.Count;
      Debug.Log(average);
      return average;
    }

    public void Clearlist()
    {
      timeBetweenShotsList.Clear();
      timeBetweenShots = 0;
    }
  }
}