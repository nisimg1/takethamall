using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OldCode
{
  public class gun : MonoBehaviour
  {
    private Vector3 maxDistance;
    private float layerMask;
    Ray ray;

    public event Action onFire = delegate { };
    [SerializeField] float timer;
    [SerializeField] float timeBetweenShot = 0.3F;
    void Update()
    {
      if (Cursor.visible == true) return;
      timer += Time.deltaTime;
      if (Input.GetMouseButtonDown(0) && timer > timeBetweenShot)
      {
        shot();
        onFire();
        timer = 0;
      }

    }


    void shot()
    {
      if (!gameManager.Instance.firstShot)
      {
        target.instance.Clearlist();
      }
      gameManager.Instance.firstShot = true;
      ray = Camera.main.ViewportPointToRay(Vector3.one / 2);
      RaycastHit[] hits;
      RaycastHit hit;
      if (Physics.Raycast(ray, out hit, 100))
      {
        if (hit.collider.GetComponent<target>())
        {
          gameManager.Instance.ShotHit();
          hit.collider.GetComponent<target>().getHit();
        }
        else
        {
          gameManager.Instance.ShotMiss();
        }
      }

    }

  }

}