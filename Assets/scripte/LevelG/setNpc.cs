﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class setNpc : MonoBehaviour
{

    public GameObject[] Enamys;

    public GameObject enemyPrefab;   
    
    public GameObject[] hostage;

    public GameObject hostagePrefab;

    [Button]
    void getAllEnameys()
    {
        Enamys = GameObject.FindGameObjectsWithTag("e");

        foreach (var vaEnamy in Enamys)
        {
            GameObject ins = Instantiate(enemyPrefab);
            ins.transform.position = vaEnamy.transform.position;
            DestroyImmediate(vaEnamy);
        }
    }

    [Button] 
        void getAllHostage()
    {
        hostage = GameObject.FindGameObjectsWithTag("h");

        foreach (var vaEnamy in hostage)
        {
           GameObject ins =  Instantiate(hostagePrefab);
           ins.transform.position = vaEnamy.transform.position;
           DestroyImmediate(vaEnamy);
        }


    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
