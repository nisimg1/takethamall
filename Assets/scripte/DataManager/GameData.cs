﻿using UnityEngine;

[CreateAssetMenu(menuName = "data")]
public class GameData : ScriptableObject
{
  private void OnEnable()
  {
    _accuracy = default;
    _shotMiss = default;
    _shotHit = default;
  }

  private void OnDisable() { ResetData(); }

  public string _name;
  public float _accuracy;
  public float _shotMiss;
  public float _shotHit;

  public void ResetData()
  {
    _accuracy = default;
    _shotMiss = default;
    _shotHit = default;
  }
}
