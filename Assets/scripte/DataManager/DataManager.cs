using Newtonsoft.Json;
using UnityEngine;
using System.IO;

public class DataManager : MonoBehaviour
{
  static PlayerStats player;

  private void Start()
  {
    CheckAndAsignPlayerData();
  }

  void CheckAndAsignPlayerData()
  {
    if (!PlayerPrefs.HasKey("player_stats"))
    {
      player = new PlayerStats("Player", 0, 80);
      PlayerPrefs.SetString("player_stats", JsonConvert.SerializeObject(player));
    }
    else
    {
      player = JsonConvert.DeserializeObject<PlayerStats>(PlayerPrefs.GetString("player_stats"));
    }
  }

  public PlayerStats GetPlayerData()
  {
    return player;
  }

  public void SetPlayerName(string newName)
  {
    player.name = newName;
    SavePlayerData();
  }

  public void SetPlayersBPM(int newBpm)
  {
    player.bpm = newBpm;
    SavePlayerData();
  }

  public void SetPlayersAccuracy(float newAccuracy)
  {
    player.accuracy = newAccuracy;
    SavePlayerData();
  }

  public void NumOfTargets(int numOfTargets)
  {
    player.targets = numOfTargets;
    SavePlayerData();
  }

  void SavePlayerData()
  {
    PlayerPrefs.SetString("player_stats", JsonConvert.SerializeObject(player));
  }

  public void ProducePlayerStatsFileToPath(string path)
  {
    WritePlayerStatsFile(path);
    //CreatePlayerStatsLocalFile();
  }

  static void WritePlayerStatsFile(string path)
  {
    StreamWriter writer = new StreamWriter(path, true);
    writer.WriteLine(player.ToString());
    writer.Close();
  }
}

public class PlayerStats
{
  public string name;
  public float accuracy;
  public int bpm;
  public int targets;
  public PlayerStats() { }
  public PlayerStats(string name, float accuracy, int bpm) { this.name = name; this.accuracy = accuracy; this.bpm = bpm; }
}