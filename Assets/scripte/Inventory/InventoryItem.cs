﻿using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName ="InventoryItem",menuName = "InventoryItem")]
public class InventoryItem : ScriptableObject
{
    [Required("you must put the name of the item")]
    public string itemName;    
    
    [PreviewField(150, ObjectFieldAlignment.Right)]
    
    [TableColumnWidth(157,Resizable = true)] 
    public Sprite icon;

    [GUIColor(0,1,0)]
    public ItemType PostiveType;
    [GUIColor(1,0,0)]
    public ItemType NagtiveType;


    [GUIColor(0,1,0)]
    public int AmmountToGive=1;
    [GUIColor(1,0,0)]
    public int AmmountToTake=-1;

    [Required("you must put the prefab of the item")]
    public GameObject Prepab;
    public void OnUse()
    {
        if (PostiveType == ItemType.Food)
        {
            playerStats.instanc.AmmountOfFoodToReduce(AmmountToGive);
            playerStats.instanc.AmmountOfWaterToReduce(AmmountToTake);
           
           
        }      
        if (PostiveType == ItemType.Water)
        {
            playerStats.instanc.AmmountOfWaterToReduce(AmmountToGive);
            playerStats.instanc.AmmountOfFoodToReduce(AmmountToTake);
        }

        if (PostiveType == ItemType.Hp)
        {
            playerStats.instanc.GetComponent<Health>().modifyHealth(AmmountToGive);
        }
       
    }



   
}
