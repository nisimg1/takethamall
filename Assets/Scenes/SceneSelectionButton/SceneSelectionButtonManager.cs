﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSelectionButtonManager : MonoBehaviour
{
  public enum SceneState { Locked, Unlocked }
  public SceneState currentState = SceneState.Unlocked;

  [SerializeField] Image lockedImg;
  [SerializeField] string sceneName;

  void Start()
  {
    SetState();
  }

  void SetState()
  {
    //todo data connect to current state
    switch (currentState)
    {
      case SceneState.Locked:
        lockedImg.gameObject.SetActive(true);
        break;
      case SceneState.Unlocked:
        lockedImg.gameObject.SetActive(false);
        break;
    }
  }

  public void OnClick()
  {
    if (currentState == SceneState.Locked) return;
    if (sceneName != null && sceneName != "")
    {
      try { SceneManager.LoadScene(sceneName); }
      catch { Debug.LogWarning("No such scene or scene not in build"); }
    }
  }
}
