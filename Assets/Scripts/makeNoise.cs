﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeNoise : MonoBehaviour
{
    [SerializeField] float NoiseDis = 5f;
    [SerializeField]AudioClip clip;
    AudioSource _audio;
    // Start is called before the first frame update
    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.incstnce.IHearSomething(transform, NoiseDis);
            _audio.PlayOneShot(clip);
        }
    }
}
