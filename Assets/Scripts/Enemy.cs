﻿
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
  idle,
  patrol,
  alert,
  chase,
  attack
}
[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
  [SerializeField] float Speed = 6;
  [SerializeField] float AlertSpeed;
  [SerializeField] float AlertviewDistance;
  [SerializeField] float viewDistance = 6;
  [SerializeField] float alermModMulti = 1.1f;
  Animator animator;
  NavMeshAgent agent;
  [SerializeField] Transform[] wayPointArry;
  Vector3 tar;
  int index = 0;
  [SerializeField] LayerMask layerMask;
  public EnemyState Stat;
  [SerializeField] float attackDis = 2f;
  [SerializeField] float DisToFlee = 10f;
  [SerializeField] GameObject Player;
  [SerializeField] float idleTimer;
  [SerializeField] float FireRate;

  [SerializeField] Transform muzzle;
  [SerializeField] GameObject bullet;
  [SerializeField] Light light;
  Color NewColor;
  [SerializeField] AudioClip shot;
  [SerializeField] AudioClip alerm;
  [SerializeField] AudioSource _audioSource1;
  [SerializeField] AudioSource _audioSource2;
  bool IsShotPlayd = false;
  bool IsAlermPlayd = false;
  private void Awake()
  {
    animator = GetComponent<Animator>();
    agent = GetComponent<NavMeshAgent>();

    if (GetComponent<Health>() != null) GetComponent<Health>().OnDie += OnOnDie;

    if (light != null)
    {
      NewColor = light.color;
    }
  }

  private void OnOnDie()
  {
    animator.enabled = false;
    this.enabled = false;
  }

  void Start()
  {
    tar = wayPointArry[0].position;
   // Stat = EnemyState.idle;

  }
  void LateUpdate()
  {
    if (Player == null) return;
    
    switch (Stat)
    {
      case EnemyState.idle:
        //Debug.Log(" i do natonig ");
        StateIdle();
        break;
      case EnemyState.patrol:
        //Debug.Log(" i need to patrol ");
        StatePatrol();
        break;
      case EnemyState.alert:
        //Debug.Log(" where are you fucker ");
        StateAlert();
        break;
      case EnemyState.chase:
        //Debug.Log(" i will take you you litle shit ");
        StateChase();
        break;
      case EnemyState.attack:
        //Debug.Log(" i told you ");
        stateAttack();
        break;
      
      default:
        break;
    }


  }

  private void stateAttack()
  {
    animator.SetBool("inMove", false);
    transform.LookAt(Player.transform.position);
    agent.Stop();
    agent.speed = 0;
    animator.SetTrigger("shot");
    if (!IsShotPlayd)
    {
      if (!_audioSource2.isPlaying)
      {
        _audioSource2.PlayOneShot(shot);
        IsShotPlayd = true;
      }
    }
    light.color = Color.red;
    AttackPlayer();
  }

  private void StateChase()
  {
    animator.SetBool("inMove", true);
    agent.Resume();
    agent.speed = Speed;
    agent.SetDestination(Player.transform.position);
    light.color = Color.red;
    if (Vector3.Distance(Player.transform.position, this.transform.position) > attackDis)
    {
      Stat =  EnemyState.attack;
    }
    if (!IsAlermPlayd)
    {
      _audioSource1.PlayOneShot(alerm);
      IsAlermPlayd = true;
    }
  }

  private void StateAlert()
  {
    animator.SetBool("inAlertMod", true);
    animator.SetBool("inMove", true);
    agent.speed = AlertSpeed;
    viewDistance = AlertviewDistance;
    agent.SetDestination(tar);
    light.color = Color.red;
    if (!IsAlermPlayd)
    {
      _audioSource1.PlayOneShot(alerm);
      IsAlermPlayd = true;
    }
  }

  private void StatePatrol()
  {
    getTarget();
    agent.speed = Speed;
    animator.SetBool("inMove", true);
    checkIfPlayerSpotted();
    agent.destination = tar;
    light.color = NewColor;
  }

  private void StateIdle()
  {
    checkIfPlayerSpotted();
    animator.SetBool("inMove", false);
    light.color = NewColor;
  }

  private void StateHandler()
  {

    if (Stat == EnemyState.idle)
    {
      idleTimer += Time.deltaTime;
      if (idleTimer > Random.Range(1, 5))
      {
        idleTimer = 0;
        Stat = EnemyState.patrol;
      }
    }
    if (Stat == EnemyState.chase && Vector3.Distance(Player.transform.position, this.transform.position) <= attackDis)
    {
      Stat = EnemyState.attack;
    }
    if (Stat == EnemyState.attack && Vector3.Distance(Player.transform.position, this.transform.position) > attackDis)
    {
      Stat = EnemyState.chase;
    }
    if (Stat == EnemyState.chase && Vector3.Distance(Player.transform.position, this.transform.position) > DisToFlee)
    {
      Stat = EnemyState.alert;
    }

    if (Stat != EnemyState.chase)
    {
      getTarget();
    }

  }

  private bool checkIfPlayerSpotted()
  {
    //Debug.Log("its me you looking for");
    int rayCount = 50;
    bool playerSpotted = false;

    RaycastHit raycastHit;
    Vector3 dirAngleS = (transform.forward.normalized + transform.right) * 3;
    Vector3 dirAngleE = (transform.forward.normalized - transform.right) * 3;




    for (int i = 0; i <= rayCount; i++)
    {

      Debug.DrawRay(transform.position + new Vector3(0,1,0), Vector3.Lerp(dirAngleS, dirAngleE, (float)i / (float)rayCount) * viewDistance + new Vector3(0,1,0), Color.blue, 0.1F);
      if (Physics.Raycast(transform.position+ new Vector3(0,1,0), Vector3.Lerp(dirAngleS, dirAngleE, (float)i / (float)rayCount) * viewDistance+ new Vector3(0,1,0), out raycastHit, viewDistance * 2, layerMask))
      {
        movement e = raycastHit.collider.gameObject.GetComponent<movement>();

        if (e != null)
        {
          playerSpotted = true;
          Debug.Log("do you se my player ");
          Stat = EnemyState.chase;
        }
      }
    }

    return playerSpotted;
  }

  void AttackPlayer()
  {
    if (Stat == EnemyState.attack)
    {
      FireRate += Time.deltaTime;
      if (FireRate > Random.Range(0.3f, 3))
      {
        FireRate = 0;
        RaycastHit hit;
        transform.LookAt(Player.transform.position);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
          Debug.Log("i try to hit you");
          var move = hit.collider.GetComponent<Health>();
          if (move)
          {
            var random = Random.Range(0, 10);
            Debug.Log("i hit you + " + hit.collider.name + random);
            move.takeHit(random);
          }
        }
      }
    }
  }
  private void getTarget()
  {
      Vector3 p1 = new Vector3(wayPointArry[index].position.x, 0, wayPointArry[index].position.z);
    Vector3 p2 = new Vector3(transform.position.x, 0, transform.position.z);
    bool reachedWaypoint = Vector3.Distance(p1, p2) < 1f;

    if (reachedWaypoint)
    {
      if (index < wayPointArry.Length -1)
      {
        index++;
      }
      else { index = 0; Stat = EnemyState.idle;
        idleTimer = 0;
      }
    }

    tar = wayPointArry[index].position;
  }
}
