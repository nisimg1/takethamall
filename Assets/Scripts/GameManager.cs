﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    List<Enemy> enemiesList = new List<Enemy>();
    Enemy[] Enemies;
    public static GameManager incstnce;


    [SerializeField] GameObject winScreen;
    [SerializeField] GameObject loseScreen;
    [SerializeField] AudioClip loseClip;
    [SerializeField] AudioClip WinClip;
    [SerializeField] AudioSource _audioSource;


    float timer = 4;
    bool isWeWin = false;
    bool isWeLose = false;
    AudioListener _AudioListener;
    [SerializeField] GameObject AudioGameObject;
    // Start is called before the first frame update
    void Start()
    {
        incstnce = this;

        _AudioListener = GetComponent<AudioListener>();
        Enemies = FindObjectsOfType<Enemy>();
        foreach (var item in Enemies)
        {
            enemiesList.Add(item);
        }
        _AudioListener.enabled = false;
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
    }

    public void IHearSomething(Transform noisedLoc, float hearingDistance)
    {
        foreach (var item in enemiesList)
        {
            float NoisedDistance = Vector3.Distance(item.gameObject.transform.position, noisedLoc.position);
            float HearingDistance = hearingDistance;


            if (NoisedDistance < HearingDistance)
            {
                item.Stat = EnemyState.alert;
                item.transform.LookAt(noisedLoc);
            }
        }
    }

    public void loseGame()
    {
        _AudioListener.enabled = true;
        AudioGameObject.SetActive(false);
        if (!_audioSource.isPlaying)
        {
            _audioSource.PlayOneShot(loseClip);
        }
        timer -= Time.deltaTime;
        loseScreen.SetActive(true);
        if (timer <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    public void winGame()
    {
        _AudioListener.enabled = true;
        AudioGameObject.SetActive(false);
        if (!_audioSource.isPlaying) 
        {
            _audioSource.PlayOneShot(WinClip); 
        }
        timer -= Time.deltaTime;
        winScreen.SetActive(true);
        if (timer <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }


}
