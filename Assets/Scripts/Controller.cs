﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class Controller : MonoBehaviour
{
    Animator anim;
    NavMeshAgent agent;
    [SerializeField] AudioClip[] stepsClip;
    AudioSource _AudioSource;
    int index = 0;
    [SerializeField] Vector3 Target = new Vector3(0, 0, 0);
    bool canPlay = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        _AudioSource = GetComponent<AudioSource>();
        Target = transform.position;
    }

    void Update()
    {
        Debug.Log(agent.velocity.sqrMagnitude);

        if (agent.velocity.sqrMagnitude > 1)
        {
            anim.SetBool("InMove", true);
            StartCoroutine(StepAudio());

        }
        else
        {
            anim.SetBool("InMove", false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Target = hit.point;
            }
        }
        agent.SetDestination(Target);

    }


    IEnumerator StepAudio()
    {

        for (int i = 0; i < stepsClip.Length; i++)
        {

            if (!_AudioSource.isPlaying)
            {
                _AudioSource.PlayOneShot(stepsClip[i]);
            }
            yield return new WaitForSeconds(0.54f);
            index++;
            if (index > stepsClip.Length)
            {
                index = 0;
            }
            canPlay = false;
        }
    }
}

